import { Injectable } from '@angular/core';
import { UserDto } from '../Entities/user.dto';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  constructor(private http: HttpClient) {}
  login(user: UserDto) {
    const url = environment.ENDPOINT.BACKEND;
    return this.http.post(url + '/login',user).toPromise();
  }
  getRegisteredUsers() {
    const url = environment.ENDPOINT.BACKEND;
    return this.http.get(url + '/obtenerUsuarios').toPromise();
  }
  register(user: UserDto) {
    const url = environment.ENDPOINT.BACKEND;
    return this.http.post(url + '/register',user).toPromise();
  }
  
}
