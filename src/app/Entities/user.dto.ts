
export class UserDto {
    nombre: string;
    email: string;
    rut: string;
    password: string;
}