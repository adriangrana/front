import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'front';
  /**
   *
   */
  constructor(private readonly router: Router) {
    
  }
  IsLogin(){
    return  Boolean(sessionStorage.getItem('IsAuth'))
  }
  logout(){
    sessionStorage.removeItem('IsAuth');
    sessionStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}
