import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { CanActivate } from '@angular/router';


@Injectable({
    providedIn: 'root'
})
export class NoAuthGuard implements CanActivate {
    constructor(private router: Router) {}
    canActivate(): any {
        if (Boolean(sessionStorage.getItem('IsAuth')) === true) {
            this.router.navigate(['/home']);
            return false;
        } else {
            return true;
        }
    }
}
