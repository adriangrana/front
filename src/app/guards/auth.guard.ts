import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, UrlTree, Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    path: ActivatedRouteSnapshot[]; route: ActivatedRouteSnapshot;
    constructor(private router: Router) {}
    canActivate(): any {
        if (Boolean(sessionStorage.getItem('IsAuth')) === true) {
            return true;
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}
