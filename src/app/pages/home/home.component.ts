import { Component, OnInit } from '@angular/core';
import { UserDto } from 'src/app/Entities/user.dto';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  users: UserDto[];
  constructor(private readonly userService:UserService) { }

  ngOnInit(): void {
    this.getUsers();
  }
  getUsers(){
    this.userService.getRegisteredUsers().then(
      (data:UserDto[])=> {
        this.users = data;
      },
      (error)=> {
        console.log(error);
      }
    )
  }

}
