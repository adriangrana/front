import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDto } from 'src/app/Entities/user.dto';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
   user: UserDto;
  

  constructor(
    private readonly router: Router,
    private readonly userService: UserService) { }
  ngOnInit(): void {
    this.user = new UserDto();
    this.user.nombre = '';
    this.user.email = '';
    this.user.rut = '';
    this.user.password = '';
  }
  login() {
    console.log(this.user);

    this.userService.login(this.user).then(
      (data: any) => {
        console.log(data);
        if (data?.token) {
          sessionStorage.setItem('IsAuth', 'true');
          sessionStorage.setItem('token', data.token);
        }
        this.router.navigate(['/home'])
      },
      (error) => {
        console.log(error);
      }
    )
  }
  register() {
    this.router.navigate(['/register'])
  }
}
