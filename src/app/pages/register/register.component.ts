import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDto } from 'src/app/Entities/user.dto';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user: UserDto;
  constructor(
    private readonly router: Router,
    private readonly userService: UserService
  ) {
    this.user = new UserDto();
    this.user.nombre = '';
    this.user.email = '';
    this.user.rut = '';
    this.user.password = '';
  }

  ngOnInit(): void {
  }
  register() {
    console.log(this.user);

    this.userService.register(this.user).then(
      (data: any) => {
        console.log(data);
        this.router.navigate(['/login'])
      },
      (error) => {
        console.log(error);
      }
    )
  }
}
